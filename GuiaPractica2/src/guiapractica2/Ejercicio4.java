
package guiapractica2;
import java.util.Scanner;

public class Ejercicio4 {

    
    public static void main(String args[]) {
        /*Escribir un algoritmo que calcule el factorial de un número ingresado
por teclado.
Tip: el factorial de un número n es el resultado de multiplicar todos
los números enteros desde 1 hasta n.
Por ejemplo, el factorial de 5 es 1 × 2 × 3 × 4 × 5 = 120. */
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese un numero para calcular el Factorial");
        int num1 = s.nextInt();
        int res = num1; 
        for (int i = 1; i < num1; i++){           
            
            res = res * (num1 - i);          
            
        }
        System.out.println("El resultado es: " + res);
    }
}
