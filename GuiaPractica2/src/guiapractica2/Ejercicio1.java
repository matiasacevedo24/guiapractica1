
package guiapractica2;
import java.util.Scanner;

public class Ejercicio1 {
    public static void main(String[] args) {
        
        /* Escribir un programa que reciba un número entero por teclado. A
continuación, mostrar la tabla de multiplicar de ese número.*/
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese un Numero");
        int num1 = scanner.nextInt();
        System.out.println("La Tabla de Multiplicar de " + num1 + " es = ");
    // calculo de la multiplicacion 
        for (int i = 1; i <= 10; i++){
            int res = i * num1; 
            System.out.println(i + " X " + num1 + " = " + res);
        }  
        
    }
    
}
