
package guiapractica2;


public class Ejercicio6 {

    
    public static void main(String args[]) {
        /* Escribir un algoritmo que imprima el listado de los números primos
menores que 200. Aclaración: el número 1 no es primo.*/
        int limite = 200;

        System.out.println("Listado de numeros primos menores que " + limite + ":");
        imprimirNumerosPrimos(limite);
    }
    // FUNCION PARA IMPRIMIR EN PANTALLA LOS NUMEROS PRIMOS 
    public static void imprimirNumerosPrimos(int limite) {
        for (int numero = 2; numero < limite; numero++) {
            if (esPrimo(numero)) {
                System.out.print(numero + " ");
            }
        }        
    }
    // FUNCION PARA VER SI LOS NUMEROS COMPRENDIDOS EN EL RANGO SON PRIMOS 
    public static boolean esPrimo(int numero) {
        if (numero <= 1) {
            return false;
        }

        for (int i = 2; i <= Math.sqrt(numero); i++) {
            if (numero % i == 0) {
                return false;
            }
        }

        return true;
    }
}

                   
    

