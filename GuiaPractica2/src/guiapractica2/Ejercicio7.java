
package guiapractica2;
import java.util.Scanner;


public class Ejercicio7 {

    
    public static void main(String args[]) {
        /* Del listado de abajo, determinar qué animal eligió el usuario mediante
la realización de tres preguntas del tipo SI/NO acerca de las tres
características elegidas (herbívoro, mamífero, doméstico). Mostrar el
resultado por pantalla.*/ 
        Scanner s = new Scanner(System.in);
        System.out.println("TE GUSTARIA UN ANIMAL HERBIVORO? y/n");
        String He = s.nextLine();
        boolean her = true;
        boolean HeB = Boolean.parseBoolean(He);
        if (HeB == true){
            her = true;
        }else if (HeB == false) {
            her = false;
        }
        System.out.println(" TE GUSTARIA UN ANIMAL MAMIFERO? y/n");
        String Mam = s.nextLine();
        boolean mam = true;
        boolean MamB = Boolean.parseBoolean(Mam);
        if (MamB == true){
            mam = true;
        }else if(MamB == false) {
            mam = false;
        }
        System.out.println("TE GSUTARIA UN ANIMAL DOMESTICO? y/n");
        String Dom = s.nextLine();
        boolean DomB = Boolean.parseBoolean(Dom );
        boolean dom = true;
        if (DomB == true){
            dom = true;
        }else if(DomB == false) {
            dom = false;
        }
        
        
        if (her == true && mam == true && dom == false){
            System.out.println("El animal seleccionado para usted es: Alce");
        }
        if (her == true && mam == true && dom == true){
            System.out.println("El animal seleccionado para usted es: Caballo");
        }
        if (her == true && mam == false && dom == false){
            System.out.println("El animal seleccionado para usted es: Caracol");
        }
        if (her == false && mam == false && dom == false){
            System.out.println("El animal seleccionado para usted es: Condor");
        }
        if (her == true && mam == true && dom == true){
            System.out.println("El animal seleccionado para usted es: Gato");
        }
        if (her == false && mam == true && dom == false){
            System.out.println("El animal seleccionado para usted es: Leon");
        }
        if (her == false && mam == false && dom == true){
            System.out.println("El animal seleccionado para usted es: Piton");
        }
        if (her == true && mam == false && dom == true){
            System.out.println("El animal seleccionado para usted es: Tortuga");
        }
    }
}
