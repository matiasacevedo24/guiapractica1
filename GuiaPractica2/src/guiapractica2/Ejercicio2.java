
package guiapractica2;

import java.util.Scanner;


public class Ejercicio2 {

    
    public static void main(String args[]) {
        /*Escribir un programa que lea una palabra por teclado y determine si
es un palíndromo. */
        
         
        // Leer la palabra por teclado
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese una palabra: ");
        String palabra = scanner.nextLine();
        
        // Eliminar los espacios en blanco y convertir a minúsculas
        palabra = palabra.replace(" ", "").toLowerCase();
        
        // Verificar si la palabra es un palíndromo
        boolean esPalindromo = true;
        int longitud = palabra.length();
        
        for (int i = 0; i < longitud / 2; i++) {
            if (palabra.charAt(i) != palabra.charAt(longitud - 1 - i)) {
                esPalindromo = false;
                break;
            }
        }
        
        // Mostrar el resultado
        if (esPalindromo) {
            System.out.println("La palabra es un polindromo.");
        } else {
            System.out.println("La palabra no es un polindromo.");
        }
    }
}

         
        
    

