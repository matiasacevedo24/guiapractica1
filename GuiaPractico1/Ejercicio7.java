
package GuiaPractico1;

import java.util.Scanner;


public class Ejercicio7 {

    
    public static void main(String args[]) {
        /*Escribir un programa que reciba el valor de dos edades y las guarde
en dos variables. Luego el programa debe intercambiar los valores de
ambas variables y mostrarlas por pantalla. Por ejemplo, si el usuario
ingresó los valores edad1 = 24 y edad2 = 35, el programa deberá
mostrar edad1 = 35 y edad2 = 24.
Tip: para intercambiar los valores de dos variables se debe utilizar
una variable auxiliar. */
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese edad1 ");
        int num1 = scanner.nextInt();
        System.out.println("Ingrese edad2 ");
        int num2 = scanner.nextInt();
      
        int num3 = num1;
        num1 = num2;
        num2 = num3;
       
     
        System.out.println("La edad1 ahora es : " + num1);
        System.out.println("La edad2 ahora es : " + num2);
      
    }
}
