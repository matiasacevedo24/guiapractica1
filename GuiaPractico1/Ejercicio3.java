
package GuiaPractico1;

import java.util.Scanner;


public class Ejercicio3 {

    
    public static void main(String args[]) {
        /* Escribir un programa que lea dos números y realice el cálculo de la
suma, la resta, la multiplicación y la división entre ambos valores. Los
resultados deben mostrarse por pantalla.*/
        Scanner scanner = new Scanner(System.in);
System.out.println("Ingrese el Primer numero");
int num1 = scanner.nextInt();
System.out.println("Ingrese el Segundo numero");
int num2 = scanner.nextInt();
// calculo de la suma 
int suma  = num1 + num2; 
int resta = num1 - num2;
int mult = num1 * num2;
int div = num1 / num2;

System.out.println("El resultado de la suma es: " + suma);
System.out.println("El resultado de la resta es: " + resta);
System.out.println("El resultado de la multiplicacion es: " + mult);
System.out.println("El resultado de la division es: " + div);

    }
}
