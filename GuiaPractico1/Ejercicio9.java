
package GuiaPractico1;

import java.util.Scanner;


public class Ejercicio9 {

    
    public static void main(String args[]) {
        /*9. A partir de una cantidad de pesos que el usuario ingresa a través del
teclado se debe obtener su equivalente en dólares, en euros, en
guaraníes y en reales. Para este ejercicio se consideran las siguientes
tasas:
1 dólar = 231,68 pesos
1 euro = 250,69 pesos
1 peso = 31,00 guaraníes
1 real = 46,81 pesos
Tip: en este ejercicio se puede usar la función printf y mostrar el
resultado con dos decimales. */
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese el importe en pesos '$' a convertir ");
        double imp = scanner.nextDouble();
        double dolar = 231.68;
        double euro = 250.69;
        double gua = 31.0;
        double real = 46.81; 
        double resd = (imp / dolar);
        double rese = (imp / euro);
        double resgu = (gua * imp); 
        double resre = (imp / real) ;
        // formula 
        System.out.printf("Dolar--> %.2f", resd);
        System.out.printf("%nEuro--> %.2f", rese);
        System.out.printf("%nGuaranies--> %.2f", resgu);
        System.out.printf("%nReal--> %.2f %n", resre);
    }
}
