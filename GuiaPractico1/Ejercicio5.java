
package GuiaPractico1;
import java.util.Scanner;

public class Ejercicio5 {

    
    public static void main(String args[]) {
        /*Pedir al usuario que ingrese el valor del radio de una circunferencia.
Calcular y mostrar por pantalla el área y el perímetro. Recordá que el
área y el perímetro se calculan con las siguientes fórmulas:
area = PI × radio²
perimetro = 2 × PI × radio
Tip: para este ejercicio se puede usar la constante matemática PI ( ) π
disponible en la clase Math.*/ 
       Scanner scanner = new Scanner(System.in);
       System.out.println("Ingrese el valor del radio de una circunferencia ");
       Double num1 = scanner.nextDouble();
      
// calculo del area 
       Double area  =  Math.PI * (Math.pow(num1, 2));
       System.out.println("El Area es: " + area);
       
// calculo del perimetro
       Double peri  =  2 * Math.PI * num1;
       System.out.println("El Perimetro es: " + peri);

    }
}
