
package GuiaPractico1;

import java.util.Scanner;


public class Ejercicio6 {

    
    public static void main(String args[]) {
        /*Pedir al usuario que ingrese el precio de un producto y el porcentaje
de descuento. A continuación mostrar por pantalla el importe
descontado y el importe a pagar.*/
        
        Scanner scanner = new Scanner(System.in);
       System.out.println("Ingrese el precio del producto  ");
       Double num1 = scanner.nextDouble();
       System.out.println("Ingrese el porcentaje de descuento  ");
       Double num2 = scanner.nextDouble();
      
// Aplicacion del descuento 
       Double desc  =  num1 * (num2/100);
       System.out.println("El descuento es de: $" + desc);
       
// Importe a pagar
       Double imp = num1 - desc;
       System.out.println("El importe a pagar es: $" + imp);
    }
}
