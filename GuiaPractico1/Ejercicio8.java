
package GuiaPractico1;

import java.util.Scanner;


public class Ejercicio8 {

    
    public static void main(String args[]) {
        /*Pedir al usuario que ingrese una temperatura en grados Celsius y
mostrar por pantalla su equivalente en kelvin y grados Fahrenheit. Las
fórmulas para conversiones son:
Kelvin = 273,15 + Celsius
Fahrenheit = 1,8 × Celsius */
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese la temperatura en grados C° ");
        double cel = scanner.nextDouble();
        double k = 273.15;
        double F = 1.8;
        
        // formula 
        double ResK = k + cel;
        double ResF = F + cel;
        System.out.println("El equivalente en Kelvin = " + ResK);
        System.out.println("El equivalente en Fahrenheit = " + ResF);
    }
}
