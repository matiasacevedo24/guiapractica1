
package guiapractica3;
//import Clases.Libro;
//import Clases.Circunferencia;
//import Clases.CuentaBancaria;
import Clases.CajaAhorro;
public class GuiaPractica3 {

    
    public static void main(String[] args) {
       
        /* PRUEBAS PARA LA CLASE LIBRO
        Clases.Libro libro1 = new Clases.Libro();
        libro1.entrada();
        libro1.toString();
        
        PRUEBAS PARA LA CLASE CIRCUNFERENCIA
        Clases.Circunferencia cir1 = new Clases.Circunferencia(10);
        System.out.println("---------------------------");
        System.out.println("El area es: " + cir1.area());
        System.out.println("---------------------------");
        System.out.println("El perimetro es: " + cir1.perimetro());
        System.out.println("---------------------------");
        
        PRUEBAS PARA LA CLASE CUENTA BANCARIA
        Clases.CuentaBancaria cu1 = new Clases.CuentaBancaria();        
        System.out.println("El dato ingresado es " + cu1.ingresar()); 
        System.out.println("--------------------------------------");
        System.out.println("El descuento es " + cu1.extraccionRapida() + "%");
        System.out.println("--------------------------------------");
        System.out.println("Su saldo actual es " + cu1.consultarSaldo());
        System.out.println("--------------------------------------");
        System.out.println(cu1.consultarDatos());*/
                
        //PRUEBAS PARA CLASES HEREDADAS 
            Clases.CajaAhorro cu1 = new Clases.CajaAhorro(); 
        System.out.println("El dato ingresado es " + cu1.ingresar()); 
        System.out.println("--------------------------------------");
        System.out.println("El descuento es $" + cu1.extraccionRapida() );
        System.out.println("--------------------------------------");
        System.out.println("Su saldo actual es $" + cu1.consultarSaldo());
        System.out.println("--------------------------------------");
        System.out.println(cu1.consultarDatos());
        System.out.println("--------------------------------------");
        System.out.println("El saldo a retirar es: $" + cu1.retirarCa());
        System.out.println("Su saldo actual es $" + cu1.consultarSaldo());
        

    }
    
    
}
