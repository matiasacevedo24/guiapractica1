
package Clases;


public class Circunferencia {
    private double radio;

    public Circunferencia(double radio) {
        this.radio = radio;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
    
    public double area(){
        double pi = 3.14;
        double area = pi * Math.pow(radio, 2);
        return area;
    }
    
    public double perimetro(){
        double pi = 3.14;
        double peri =  2 * pi * radio;
        return peri;
    }
}
