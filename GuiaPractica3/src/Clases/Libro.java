
package Clases;

import java.util.Scanner;


public class Libro {
    int isbn;
    String titulo;
    String autor;
    int numeroPag;
    // constructor 

    public Libro() {
    }
    
    public Libro(int isbn, String titulo, String autor, int numeroPag) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.autor = autor;
        this.numeroPag = numeroPag;
    }
    
    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getNumeroPag() {
        return numeroPag;
    }

    public void setNumeroPag(int numeroPag) {
        this.numeroPag = numeroPag;
    }
    
    // metodos 
    
    public void entrada(){
       Scanner s = new Scanner(System.in);
       System.out.println("Ingrese el codido ISBN: ");
       setIsbn(s.nextInt());       
       s.nextLine();
       System.out.println("Ingrese el Titulo del Libro: ");
       setTitulo(s.nextLine());
       System.out.println("Ingrese el Autor del Libro: ");
       setAutor(s.nextLine());
       System.out.println("Ingrese la cantidad de Paginas que contiene el Libro ");
       setNumeroPag(s.nextInt());   
       s.close();
    }

   // @Override
    public String toString() {
       System.out.println("--------------------------------------------");  
       System.out.println("El codido ISBN es : " + isbn); 
       System.out.println("El Titulo del Libro es: " + titulo);      
       System.out.println("El Autor del Libro es: " + autor);       
       System.out.println("La cantidad de Paginas que contiene el Libro es: " + numeroPag);
       
        
       return "";
    }
    
}



