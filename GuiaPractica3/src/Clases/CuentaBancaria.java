
package Clases;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;
public class CuentaBancaria {
    int numero;
    long dni;
    double saldo;

    public CuentaBancaria() {
    }

    public CuentaBancaria(int numero, long dni, double saldo) {
        this.numero = numero;
        this.dni = dni;
        this.saldo = saldo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public long getDni() {
        return dni;
    }

    public void setDni(long dni) {
        this.dni = dni;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    // metodos
    
    public double ingresar() {
        Scanner in = new Scanner(System.in);
        System.out.println("Ingrese el monto ");
        double num = in.nextDouble();
        if (num > 0 ){
           saldo = num + saldo; 
        } 
        return saldo ;
    } 
    
    public double extraccionRapida() {
        double desc = (saldo * 20) /100;
        saldo =  saldo - desc ;
        return desc;
    }
    public double consultarSaldo(){
        return saldo;
    }
    
    public Map<String, Object> consultarDatos(){
        Map<String, Object> resultado = new HashMap<>();
        resultado.put("Numero ", numero);
        resultado.put("DNI ", dni);
        resultado.put("Saldo Actual ", saldo);
        return resultado;
    }
}
