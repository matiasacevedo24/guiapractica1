/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Clases;

import java.util.Scanner;

/**
 *
 * @author Matias
 */
public class CuentaCorriente extends CuentaBancaria {
    
    // METODOS
    public double retirarCC(){
        Scanner in = new Scanner(System.in);
        System.out.println("Ingrese el monto a retirar: ");
        double num = in.nextDouble();
        if (num > 0 ){
           saldo = num - saldo; 
        } 
        return num ;
    }
    
    
    
}
