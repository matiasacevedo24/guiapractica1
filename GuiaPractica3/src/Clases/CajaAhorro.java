
package Clases;

import java.util.Scanner;


public class CajaAhorro extends CuentaBancaria {
    

    public CajaAhorro() {
    }

    public CajaAhorro(int numero, long dni, double saldo) {
        super(numero, dni, saldo);
    }
    
    // metodos
    
    public double retirarCa(){
        Scanner in = new Scanner(System.in);
        System.out.println("Ingrese el monto a retirar: ");
        double num = in.nextDouble();
        double resultado = 0;
        if (num > 0 && num > saldo){
           System.out.println("El importe supero el saldo enn la caja ");
           resultado = saldo; 
                      
        } else if (num > 0 && num <= saldo) {
            resultado = saldo - num ; 
        }
        return resultado;
    }
}
